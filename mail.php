<?php

 // An welche Adresse sollen die Mails gesendet werden?
 $zieladresse = 'contact@silas229.pe.hu';

 // Welche Adresse soll als Absender angegeben werden?
 // (Manche Hoster lassen diese Angabe vor dem Versenden der Mail ueberschreiben)
 $absenderadresse = 'kontaktformular@silas229.pe.hu';

 // Welcher Absendername soll verwendet werden?
 $absendername = 'Kontaktformular von der Website';

 // Welchen Betreff sollen die Mails erhalten?
 $betreff = 'Neue Nachricht';

 // Zu welcher Seite soll als "Danke-Seite" weitergeleitet werden?
 // Wichtig: Sie muessen hier eine gueltige HTTP-Adresse angeben!
 $urlDankeSeite = 'http://silas229.pe.hu/sent/';

 // Welche(s) Zeichen soll(en) zwischen dem Feldnamen und dem angegebenen Wert stehen?
 $trenner = ":\t"; // Doppelpunkt + Tabulator

 /**
  * Ende Konfiguration
  */

 require_once "swiftmailer-5.x/lib/swift_required.php"; // Swift initialisieren

 if ($_SERVER['REQUEST_METHOD'] === "POST") {

     $message = Swift_Message::newInstance(); // Ein Objekt für die Mailnachricht.

     $message
         ->setFrom(array($absenderadresse => $absendername))
         ->setTo(array($zieladresse)) // alternativ existiert setCc() und setBcc()
         ->setSubject($betreff);

     $mailtext = "";

     foreach ($_POST as $name => $wert) {
         if (is_array($wert)) {
                 foreach ($wert as $einzelwert) {
                 $mailtext .= $name.$trenner.$einzelwert."\n";
             }
         } else {
             $mailtext .= $name.$trenner.$wert."\n";
         }
     }

     $message->setBody($mailtext, 'text/plain');

     $mailer = Swift_Mailer::newInstance(Swift_MailTransport::newInstance());
     $result = $mailer->send($message);

     if ($result == 0) {
         die("Mail konnte nicht versandt werden.");
     }

     header("Location: $urlDankeSeite");
     exit;
 }

 header("Content-type: text/html; charset=utf-8");

?>
<!DOCTYPE html>
<html>
<head>
    <title>Nachricht wird versendet!</title>
    <link rel="icon" href="/lib/img/icon.png">
</head>
<body>

</body>
</html>
